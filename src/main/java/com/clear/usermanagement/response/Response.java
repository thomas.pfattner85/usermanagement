package com.clear.usermanagement.response;

import lombok.Data;

@Data
public class Response {
    
    public ResponseList list = new ResponseList();
    public ResponseObject object = new ResponseObject();
    
}
