package com.clear.usermanagement.response;

import java.util.Optional;
import lombok.Data;

@Data
public class ResponseObject {
    
    private String status;
    private Optional<?> data;
    private String message;
    
}
