package com.clear.usermanagement.response.account;

import java.util.List;
import lombok.Data;

@Data
public class AccountResponse {
    
    private Integer account_id;
    private String account_uuid;
    private String username;
    private String email;
    private String fullname;
    private List<Integer> role_id;
    private Integer jobtitle_id;
    private String jobtitle;
    private String token;
    private String token_mobile;
    private String token_expired;
    private String token_expired_mobile;
    private Integer activated;
    private Integer banned;
    private String ban_reason;
    private Integer company_id;
    private String company;
    private String last_action;
    
}
