package com.clear.usermanagement.response.auth;

import java.util.List;
import lombok.Data;

@Data
public class LoginResponse {
    
    private Integer user_id;
    private String username;
    private String email;
    private List<Integer> role_id;
    private String token;
    private String token_mobile;
    private String token_expired;
    private String token_expired_mobile;
    private Integer company_id;
    private String last_action;

    public LoginResponse(Integer user_id, String username, String email, List<Integer> role_id, String token, String token_mobile, String token_expired, String token_expired_mobile, Integer company_id, String last_action) {
        this.user_id = user_id;
        this.username = username;
        this.email = email;
        this.role_id = role_id;
        this.token = token;
        this.token_mobile = token_mobile;
        this.token_expired = token_expired;
        this.token_expired_mobile = token_expired_mobile;
        this.company_id = company_id;
        this.last_action = last_action;
    }
    
}
