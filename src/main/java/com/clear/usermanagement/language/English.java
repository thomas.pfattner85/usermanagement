package com.clear.usermanagement.language;

public class English {

    // Status
    public static String success = "success";
    public static String failed = "failed";

    // Error
    public static String internalServerError = "Internal server error";
    public static String notFoundError = "Data not found";
    public static String forbiddenError = "Access forbidden";

    // Config
    public static String tokenConfigError = "You must be config your token first";
    public static String apiManagementError = "You must be config your api management first";

    // Exist
    public static String userExist = "User already exist";

    // Not Null
    public static String usernameNull = "Username cannot be empty";
    public static String emailNull = "Email cannot be empty";
    public static String passwordNull = "Password cannot be empty";
    public static String companyIdNull = "Company ID cannot be empty";
    public static String accountIdNull = "Account ID cannot be empty";
    public static String tokenNull = "Token cannot be empty";

    // Not Found
    public static String accountNotFound = "The email address provided is not registered. Please contact your Administrator";
    public static String userNotFound = "User not found";
    public static String roleNotFound = "Role not found";

    // Not Allowed
    public static String ipNotAllowed = "IP address not allowed";
    public static String registerNotAllowed = "Company doesn't allow to register user";

    // Not Activated
    public static String userNotActivated = "User not activated yet";
    
    // Already
    public static String userAlreadyActivated = "User already activated";

    // Banned
    public static String userBanned = "User already banned";

    // Incorrect
    public static String incorrectPassword = "Incorrect password";
    
    // Expired
    public static String tokenExpired = "Token expired";
    public static String actionExpired = "Action expired";
    
    // Valid
    public static String tokenValid = "Token valid";
    public static String actionValid = "Action valid";
    
    // Invalid
    public static String tokenInvalid = "Token invalid";
    
    // Doesn't Have Access
    public static String userDoesntHaveAccess = "User doesn't have access";

    // Success
    public static String superadminSuccess = "Success to create superadmin";
    public static String getDataSuccess = "Get data success";
    public static String registerSuccess = "Register account success";
    public static String loginSuccess = "Login success";
    public static String activatedAccountSuccess = "Activated account success";
    public static String createDataSuccess = "Create data success";

    // Failed
    public static String superadminFailed = "Failed to create superadmin account";
    public static String getDataFailed = "Get data failed";
    public static String registerFailed = "Registered account failed";
    public static String loginFailed = "Login failed";
    public static String activatedAccountFailed = "Activated account failed";

}
