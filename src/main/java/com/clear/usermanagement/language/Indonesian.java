package com.clear.usermanagement.language;

public class Indonesian {
    
    // Status
    public static String success = "sukses";
    public static String failed = "gagal";
    
    // Error
    public static String internalServerError = "Internal server error";
    public static String notFoundError = "Data tidak ditemukan";
    public static String forbiddenError = "Access forbidden";
    
    // Config
    public static String tokenConfigError = "Anda harus melakukan konfigurasi token terlebih dahulu";
    
    // Exist
    public static String userExist = "Data user sudah ada";
    
    // Not Null
    public static String usernameNull = "Username tidak boleh kosong";
    public static String emailNull = "Email tidak boleh kosong";
    public static String passwordNull = "Password tidak boleh kosong";

    // Not Found
    public static String accountNotFound = "Email yang anda masukkan tidak terdaftar. Silahkan kontak Administrator anda";
    public static String userNotFound = "User tidak ditemukan";
    public static String roleNotFound = "Role tidak ditemukan";
    
    // Not Allowed
    public static String ipNotAllowed = "Alamat IP tidak diizinkan";
    
    // Not Activated
    public static String userNotActivated = "User belum melakukan aktivasi";
    
    // Banned
    public static String userBanned = "User sudah di banned";

    // Incorrect
    public static String incorrectPassword = "Password salah";

    // Success
    public static String superadminSuccess = "Sukses membuat akun superadmin";
    public static String getDataSuccess = "Sukses mengambil data";

    // Failed
    public static String superadminFailed = "Gagal membuat akun superadmin";
    public static String getDataFailed = "Gagal mengambil data";
    
}
