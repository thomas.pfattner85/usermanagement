package com.clear.usermanagement.controller.console.auth;

import com.clear.usermanagement.language.English;
import com.clear.usermanagement.request.auth.ActivatedAccountRequest;
import com.clear.usermanagement.request.auth.CreateSuperadminRequest;
import com.clear.usermanagement.request.auth.LoginRequest;
import com.clear.usermanagement.request.auth.RegisterRequest;
import com.clear.usermanagement.response.Response;
import com.clear.usermanagement.service.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.clear.usermanagement.service.CreateSuperadminService;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/api")
public class AuthControllerConsole {

    private final CreateSuperadminService createSuperadminService;
    private final AuthService authService;

    @Autowired
    public AuthControllerConsole(
            CreateSuperadminService createSuperadminService,
            AuthService authService
    ) {
        this.createSuperadminService = createSuperadminService;
        this.authService = authService;
    }

    @PostMapping("/create_superadmin")
    public ResponseEntity createSuperadmin(@Valid @RequestBody CreateSuperadminRequest request, Errors errors, BindingResult result) {
        Response response = new Response();
        response.object.setStatus(English.failed);

        if (errors.hasErrors()) {
            FieldError errorUsername = result.getFieldError("username");
            FieldError errorEmail = result.getFieldError("email");
            FieldError errorPassword = result.getFieldError("password");

            if (errorUsername != null) {
                response.object.setMessage(English.usernameNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorEmail != null) {
                response.object.setMessage(English.emailNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorPassword != null) {
                response.object.setMessage(English.passwordNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }
        }

        try {
            return createSuperadminService.createSuperadmin(response, request);
        } catch (JsonProcessingException e) {
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/login")
    public ResponseEntity login(@Valid @RequestBody LoginRequest request, Errors errors, BindingResult result) {
        Response response = new Response();
        response.object.setStatus(English.failed);

        if (errors.hasErrors()) {
            FieldError errorUsername = result.getFieldError("username");
            FieldError errorPassword = result.getFieldError("password");

            if (errorUsername != null) {
                response.object.setMessage(English.usernameNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorPassword != null) {
                response.object.setMessage(English.passwordNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }
        }

        try {
            return authService.login(response, request);
        } catch (JsonProcessingException e) {
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/register")
    public ResponseEntity register(@Valid @RequestBody RegisterRequest request, Errors errors, BindingResult result) {
        Response response = new Response();
        response.object.setStatus(English.failed);

        if (errors.hasErrors()) {
            FieldError errorUsername = result.getFieldError("username");
            FieldError errorPassword = result.getFieldError("password");
            FieldError errorEmail = result.getFieldError("email");
            FieldError errorCompanyId = result.getFieldError("company_id");

            if (errorUsername != null) {
                response.object.setMessage(English.usernameNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorPassword != null) {
                response.object.setMessage(English.passwordNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorEmail != null) {
                response.object.setMessage(English.emailNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorCompanyId != null) {
                response.object.setMessage(English.companyIdNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }
        }

        try {
            return authService.register(response, request);
        } catch (JsonProcessingException e) {
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/activated_account")
    public ResponseEntity activatedAccount(@Valid @RequestBody ActivatedAccountRequest request, Errors errors, BindingResult result) {
        Response response = new Response();
        response.object.setStatus(English.failed);

        if (errors.hasErrors()) {
            FieldError errorAccountId = result.getFieldError("account_id");
            FieldError errorCompanyId = result.getFieldError("company_id");

            if (errorAccountId != null) {
                response.object.setMessage(English.accountIdNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorCompanyId != null) {
                response.object.setMessage(English.companyIdNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }
        }

        try {
            return authService.activatedAccount(response, request);
        } catch (JsonProcessingException e) {
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
