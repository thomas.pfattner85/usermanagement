package com.clear.usermanagement.controller.console.example;

import com.clear.usermanagement.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ResponseController {
    
    @GetMapping("/response/list")
    public ResponseEntity<?> responseList() {
        Response response = new Response();
        
        return new ResponseEntity(response.list, HttpStatus.OK);
    }
    
    @GetMapping("/response/object")
    public ResponseEntity<?> responseObject() {
        Response response = new Response();
        
        return new ResponseEntity(response.object, HttpStatus.OK);
    }
    
}
