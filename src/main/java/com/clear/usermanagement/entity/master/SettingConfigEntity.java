package com.clear.usermanagement.entity.master;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "ref_setting_config")
public class SettingConfigEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "setting_config_id", length = 11, nullable = false)
    private Integer settingConfigId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;
    
    @NotNull
    @Column(name = "company_id", length = 11, nullable = false)
    private Integer companyId;
    
    @NotBlank
    @Column(name = "logo_lg", length = 50, nullable = false)
    @Type(type = "text")
    private String logoLg;
    
    @NotBlank
    @Column(name = "logo_md", length = 50, nullable = false)
    @Type(type = "text")
    private String logoMd;
    
    @NotBlank
    @Column(name = "logo_sm", length = 50, nullable = false)
    @Type(type = "text")
    private String logoSm;
    
    @NotBlank
    @Column(name = "main_theme", length = 100, nullable = false)
    private String mainTheme;
    
    @NotBlank
    @Column(name = "navbar_theme", length = 100, nullable = false)
    private String navbarTheme;
    
    @NotBlank
    @Column(name = "toolbar_theme", length = 100, nullable = false)
    private String toolbarTheme;
    
    @NotBlank
    @Column(name = "footer_theme", length = 100, nullable = false)
    private String footerTheme;
    
    @NotBlank
    @Column(name = "layout", length = 100, nullable = false)
    private String layout;
    
    @NotBlank
    @Column(name = "languages", length = 25, nullable = false)
    private String languages;
    
    @NotBlank
    @Column(name = "direction", length = 25, nullable = false)
    private String direction;

    @NotNull
    @Column(name = "custom_scrollbar", length = 50, nullable = false)
    private Boolean customScrollbar;
    
    @NotNull
    @Column(name = "animation", length = 50, nullable = false)
    private Boolean animation;
    
    @NotNull
    @Column(name = "created_by", length = 25, nullable = false)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    
}
