package com.clear.usermanagement.entity.master;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "ref_jobtitle")
public class JobtitleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "jobtitle_id", length = 11, nullable = false)
    private Integer jobtitleId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;
    
    @NotBlank
    @Column(name = "jobtitle_name", length = 100, nullable = false)
    private String jobtitleName;
    
    @NotNull
    @Column(name = "mandays", length = 100, nullable = false)
    private Integer mandays;
    
    @NotNull
    @Column(name = "manmonth", length = 100, nullable = false)
    private Integer manmonth;
    
    @NotNull
    @Column(name = "company_id", length = 11, nullable = false)
    private Integer companyId;
    
    @NotNull
    @Column(name = "status", length = 1, nullable = false, columnDefinition = "integer default 1")
    private Integer status;
    
    @NotNull
    @Column(name = "created_by", length = 25, nullable = false)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    
}
