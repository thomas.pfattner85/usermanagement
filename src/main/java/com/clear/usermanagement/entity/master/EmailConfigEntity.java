package com.clear.usermanagement.entity.master;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "ref_email_config")
public class EmailConfigEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "email_config_id", length = 11, nullable = false)
    private Integer emailConfigId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;
    
    @NotBlank
    @Column(name = "protocol", length = 50, nullable = false)
    private String protocol;
    
    @NotBlank
    @Column(name = "smtp_host", length = 50, nullable = false)
    private String smtpHost;
    
    @NotBlank
    @Column(name = "smtp_port", length = 25, nullable = false)
    private String smtpPort;
    
    @NotBlank
    @Column(name = "smtp_user", length = 100, nullable = false)
    private String smtpUser;
    
    @NotBlank
    @Column(name = "smtp_password", length = 100, nullable = false)
    private String smtpPassword;
    
    @NotBlank
    @Column(name = "mail_type", length = 25, nullable = false)
    private String mailType;
    
    @NotBlank
    @Column(name = "charset", length = 25, nullable = false)
    private String charset;
    
    @NotNull
    @Column(name = "wordwrap", length = 11, nullable = false)
    private Boolean wordwrap;
    
    @NotNull
    @Column(name = "company_id", length = 11, nullable = false)
    private Integer companyId;
    
    @NotNull
    @Column(name = "created_by", length = 25, nullable = false)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    
}
