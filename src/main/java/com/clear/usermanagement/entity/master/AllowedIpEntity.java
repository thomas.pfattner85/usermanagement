package com.clear.usermanagement.entity.master;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "ref_allowed_ip")
public class AllowedIpEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "allowed_ip_id", length = 11, nullable = false)
    private Integer allowedIpId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;

    @NotBlank
    @Column(name = "allowed_ip", length = 50, nullable = false)
    private String allowedIp;

    @NotNull
    @Column(name = "company_id", length = 11, nullable = false)
    private Integer companyId;

    @NotNull
    @Column(name = "status", length = 1, nullable = false, columnDefinition = "integer default 1")
    private Integer status;

    @NotNull
    @Column(name = "created_by", length = 25, nullable = false)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updateAt;

}
