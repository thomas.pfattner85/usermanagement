package com.clear.usermanagement.entity.master;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "ref_token_config")
public class TokenConfigEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "token_config_id", length = 11, nullable = false)
    private Integer tokenConfigId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;

    @NotBlank
    @Column(name = "private_key", length = 50, nullable = false)
    private String privateKey;

    @NotBlank
    @Column(name = "private_key_mobile", length = 50, nullable = false)
    private String privateKeyMobile;

    @NotNull
    @Column(name = "expired", length = 11, nullable = false)
    private Integer expired;

    @NotNull
    @Column(name = "expired_mobile", length = 11, nullable = false)
    private Integer expiredMobile;

    @NotBlank
    @Column(name = "type_expired", length = 50, nullable = false)
    private String typeExpired;

    @NotBlank
    @Column(name = "type_expired_mobile", length = 50, nullable = false)
    private String typeExpiredMobile;

    @NotNull
    @Column(name = "is_multi_login", length = 11, nullable = false)
    private Boolean isMultiLogin;

    @NotBlank
    @Column(name = "expired_from", length = 50, nullable = false, columnDefinition = "varchar(50) default 'token'")
    private String expiredFrom;

    @NotNull
    @Column(name = "company_id", length = 11, nullable = false)
    private Integer companyId;

    @NotNull
    @Column(name = "created_by", length = 25, nullable = false)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updatedAt;

}
