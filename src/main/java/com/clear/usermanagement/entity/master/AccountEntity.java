package com.clear.usermanagement.entity.master;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "ref_account")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id", length = 100, nullable = false)
    private Integer accountId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;

    @NotBlank
    @Column(name = "username", length = 50, nullable = false)
    private String username;

    @NotBlank
    @Column(name = "email", length = 100, nullable = false)
    private String email;

    @NotBlank
    @Column(name = "password", length = 255, nullable = false)
    private String password;

    @NotBlank
    @Column(name = "admin_password", length = 255, nullable = true)
    private String adminPassword;

    @Column(name = "token", length = 255, nullable = true)
    @Type(type = "text")
    private String token;

    @Column(name = "token_mobile", length = 255, nullable = true)
    @Type(type = "text")
    private String tokenMobile;

    @Column(name = "token_expired", length = 25, nullable = true)
    private String tokenExpired;

    @Column(name = "token_expired_mobile", length = 25, nullable = true)
    private String tokenExpiredMobile;

    @Column(name = "fcm", nullable = true)
    @Type(type = "text")
    private String fcm;

    @Column(name = "jobtitle_id", length = 11, nullable = true)
    private Integer jobtitleId;

    @Column(name = "company_id", length = 11, nullable = true)
    private Integer companyId;

    @Column(name = "last_action", length = 25, nullable = true)
    private String lastAction;

    @NotNull
    @Column(name = "status", length = 1, nullable = false, columnDefinition = "integer default 1")
    private Integer status;

    @NotNull
    @Column(name = "activated", length = 1, nullable = false, columnDefinition = "integer default 1")
    private Integer activated;

    @NotNull
    @Column(name = "banned", length = 1, nullable = false, columnDefinition = "integer default 0")
    private Integer banned;

    @NotNull
    @Column(name = "is_reset_password", length = 11, nullable = false, columnDefinition = "integer default 1")
    private Integer isResetPassword;

    @Column(name = "ban_reason", length = 255, nullable = true)
    @Type(type = "text")
    private String banReason;

    @Column(name = "created_by", length = 25, nullable = true)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updatedAt;

}
