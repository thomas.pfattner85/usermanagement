package com.clear.usermanagement.entity.transaction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Data
@Table(name = "dat_user_fcm")
public class UserFcmEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_fcm_id", length = 11, nullable = false)
    private Integer userFcmId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;
    
    @NotNull
    @Column(name = "account_id", length = 11, nullable = false)
    private Integer accountId;
    
    @NotBlank
    @Column(name = "fcm", nullable = false)
    @Type(type = "text")
    private String fcm;
    
}
