package com.clear.usermanagement.entity.transaction;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "dat_menu_role")
public class MenuRoleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "menu_role_id", length = 11, nullable = false)
    private Integer menuRoleId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;
    
    @NotNull
    @Column(name = "menu_company_id", length = 11, nullable = false)
    private Integer menuCompanyId;
    
    @NotNull
    @Column(name = "role_id", length = 11, nullable = false)
    private Integer roleId;
    
    @Column(name = "is_view", length = 11, nullable = false)
    private Boolean isView;
    
    @Column(name = "is_create", length = 11, nullable = false)
    private Boolean isCreate;
    
    @Column(name = "is_edit", length = 11, nullable = false)
    private Boolean isEdit;
    
    @Column(name = "is_delete", length = 11, nullable = false)
    private Boolean isDelete;
    
    @NotNull
    @Column(name = "created_by", length = 25, nullable = false)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    
}
