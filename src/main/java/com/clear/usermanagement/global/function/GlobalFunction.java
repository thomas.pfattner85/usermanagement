package com.clear.usermanagement.global.function;

import com.clear.usermanagement.entity.master.AccountEntity;
import com.clear.usermanagement.entity.master.ActivityConfigEntity;
import com.clear.usermanagement.entity.master.AllowedIpConfigEntity;
import com.clear.usermanagement.entity.master.AllowedIpEntity;
import com.clear.usermanagement.entity.master.ApiManagementEntity;
import com.clear.usermanagement.entity.master.CompanyEntity;
import com.clear.usermanagement.entity.master.RoleEntity;
import com.clear.usermanagement.entity.master.TokenConfigEntity;
import com.clear.usermanagement.language.English;
import com.clear.usermanagement.global.query.GetQuery;
import com.clear.usermanagement.repository.master.AccountRepository;
import com.clear.usermanagement.repository.master.ActivityConfigRepository;
import com.clear.usermanagement.repository.master.AllowedIpConfigRepository;
import com.clear.usermanagement.repository.master.AllowedIpRepository;
import com.clear.usermanagement.repository.master.CompanyRepository;
import com.clear.usermanagement.repository.master.TokenConfigRepository;
import com.clear.usermanagement.response.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class GlobalFunction {

    public final ServiceFunction serviceFunction;
    public final GetFunction getFunction;
    public final GetQuery getQuery;
    public final HttpServletRequest httpServletRequest;
    public final AccountRepository accountRepository;
    public final ActivityConfigRepository activityConfigRepository;
    public final AllowedIpRepository allowedIpRepository;
    public final AllowedIpConfigRepository allowedIpConfigRepository;
    public final CompanyRepository companyRepository;
    public final TokenConfigRepository tokenConfigRepository;

    @Autowired
    public GlobalFunction(
            ServiceFunction serviceFunction,
            GetFunction getFunction,
            GetQuery getQuery,
            HttpServletRequest httpServletRequest,
            AccountRepository accountRepository,
            ActivityConfigRepository activityConfigRepository,
            AllowedIpRepository allowedIpRepository,
            AllowedIpConfigRepository allowedIpConfigRepository,
            CompanyRepository companyRepository,
            TokenConfigRepository tokenConfigRepository
    ) {
        this.serviceFunction = serviceFunction;
        this.getFunction = getFunction;
        this.getQuery = getQuery;
        this.httpServletRequest = httpServletRequest;
        this.accountRepository = accountRepository;
        this.activityConfigRepository = activityConfigRepository;
        this.allowedIpRepository = allowedIpRepository;
        this.allowedIpConfigRepository = allowedIpConfigRepository;
        this.companyRepository = companyRepository;
        this.tokenConfigRepository = tokenConfigRepository;
    }

    public Boolean isSuperadmin(Integer accountId) throws ParseException {
        RoleEntity roleAdmin = getFunction.getRoleSuperadmin();
        List<Integer> role = getFunction.getRoleIdByAccountId(accountId);

        if (roleAdmin == null) {
            return false;
        }

        return role.contains(roleAdmin.getRoleId());
    }

    public Boolean isPermitted(HttpServletRequest request, Integer menuId, String action, Integer companyId, String typeToken) throws ParseException {
        String token = request.getHeader("token");
        AccountEntity account = getFunction.getTokenAccount(typeToken, token);

        List<Integer> roleId = getFunction.getRoleIdByAccountId(account.getAccountId());
        List<String> privilegesMenuButton = getQuery.getPrivilegesMenuButtonByRoleId(roleId);

        Boolean isMyCompany = true;
        if (companyId != null) {
            if (!Objects.equals(account.getCompanyId(), companyId)) {
                isMyCompany = false;
            }
        }

        return (privilegesMenuButton.contains(action) && isMyCompany);
    }

    public Boolean checkUsername(String username, Integer companyId) {
        AccountEntity account = companyId == null
                ? accountRepository.findByUsernameAdmin(username)
                : accountRepository.findByUsername(username, companyId);

        if (account == null) {
            return false;
        }

        return true;
    }

    public Boolean checkUsernamePassword(String username, String password, Integer companyId) {
        AccountEntity account = companyId == null
                ? accountRepository.checkUserAdmin(username, password)
                : accountRepository.checkUser(username, password, companyId);

        if (account == null) {
            return false;
        }

        return true;
    }

    public Boolean checkAccountExist(String username, String email, Integer companyId) {
        AccountEntity account = accountRepository.findByUsername(username, companyId) == null
                ? accountRepository.findByUsername(email, companyId)
                : accountRepository.findByUsername(username, companyId);

        if (account == null) {
            return false;
        }

        return true;
    }

    public Boolean checkAccountExistByAccountId(Integer accountId, Integer companyId) {
        AccountEntity account = accountRepository.findByUserActivatedByAccountId(accountId, companyId);

        if (account == null) {
            return false;
        }

        return true;
    }

    public Boolean checkAccountActivatedByAccountId(Integer accountId, Integer companyId) {
        AccountEntity account = accountRepository.findByUserNotActivatedByAccountId(accountId, companyId);

        if (account == null) {
            return false;
        }

        return true;
    }

    public Boolean checkAccountBannedByAccountId(Integer accountId, Integer companyId) {
        AccountEntity account = accountRepository.findByUserBannedByAccountId(accountId, companyId);

        if (account != null) {
            return false;
        }

        return true;
    }

    public Boolean checkIsRegisterAccount(Integer companyId) {
        CompanyEntity company = companyRepository.findByCompanyId(companyId);

        if (company == null) {
            return false;
        }

        return company.getRegisterUser();
    }

    public Boolean checkActivityConfig(Integer accountId, Integer companyId) throws ParseException {
        Boolean isSuperadmin = isSuperadmin(accountId);

        if (isSuperadmin) {
            return true;
        } else {
            ActivityConfigEntity activityConfig = activityConfigRepository.findByCompanyId(companyId);
            return activityConfig == null ? false : activityConfig.getStatus() == 1;
        }
    }

    public Boolean checkAllowedIp(String ipAddress, Integer accountId, Integer companyId) throws ParseException {
        Boolean isSuperadmin = isSuperadmin(accountId);
        Boolean checkAllowedIp = false;

        if (isSuperadmin) {
            checkAllowedIp = false;
        } else {
            AllowedIpConfigEntity allowedIpConfig = allowedIpConfigRepository.findByCompanyId(companyId);
            checkAllowedIp = allowedIpConfig == null ? false : allowedIpConfig.getStatus() == 1;
        }

        if (checkAllowedIp) {
            List<AllowedIpEntity> listAllowedIp = allowedIpRepository.findByCompanyId(companyId);
            List<String> allowedIp = new ArrayList<>();

            listAllowedIp.forEach((item) -> {
                if (!allowedIp.contains(item.getAllowedIp())) {
                    allowedIp.add(item.getAllowedIp());
                }
            });

            return allowedIp.contains(ipAddress);
        } else {
            return true;
        }
    }

    public ResponseEntity<?> checkToken(HttpServletRequest request, Response response, Integer companyId, String url) throws ParseException {
        String token = request.getHeader("token");
        String ip = request.getRemoteAddr();

        if (token != null) {
            if (!token.isEmpty()) {
                AccountEntity checkToken = accountRepository.findByToken(token);
                if (checkToken != null) {
                    if (!checkAllowedIp(ip, checkToken.getAccountId(), checkToken.getCompanyId())) {
                        response.object.setMessage(English.ipNotAllowed);
                        return new ResponseEntity(response.object, HttpStatus.OK);
                    }

                    if (isSuperadmin(checkToken.getAccountId())) {
                        String stringDate = checkToken.getTokenExpired();
                        Date tokenExpired = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(stringDate);
                        Date date = new Date();

                        if (tokenExpired == null) {
                            response.object.setMessage(English.tokenExpired);
                            return new ResponseEntity(response.object, HttpStatus.OK);
                        }

                        if (date.compareTo(tokenExpired) > 0) {
                            response.object.setMessage(English.tokenExpired);
                            return new ResponseEntity(response.object, HttpStatus.OK);
                        } else {
                            serviceFunction.generateLastAction(checkToken.getAccountId(), checkToken.getCompanyId());
//                            response.object.setStatus(English.success);
//                            response.object.setMessage(English.tokenValid);
                            return new ResponseEntity(response.object, HttpStatus.CONTINUE);
                        }

                    } else {
                        ApiManagementEntity apiManagement = getFunction.getApiManagementUrl(url);
                        if (apiManagement == null) {
                            response.object.setMessage(English.apiManagementError);
                            return new ResponseEntity(response.object, HttpStatus.OK);
                        }

                        Integer menuId = getFunction.getMenuId(apiManagement.getMenuClass());
                        String action = apiManagement.getButtonClass();

                        TokenConfigEntity tokenConfig = tokenConfigRepository.findByCompanyId(checkToken.getCompanyId());
                        String stringDate = "token".equals(tokenConfig.getExpiredFrom()) ? checkToken.getTokenExpired() : checkToken.getLastAction();
                        Date tokenExpired = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(stringDate);
                        Date date = new Date();

                        if ("token".equals(tokenConfig.getExpiredFrom())) {
                            if (tokenExpired == null) {
                                response.object.setMessage(English.tokenExpired);
                                return new ResponseEntity(response.object, HttpStatus.OK);
                            }

                            if (date.compareTo(tokenExpired) > 0) {
                                response.object.setMessage(English.tokenExpired);
                                return new ResponseEntity(response.object, HttpStatus.OK);
                            } else {
                                Boolean isPermitted = isPermitted(request, menuId, action, companyId, "console");
                                serviceFunction.generateLastAction(checkToken.getAccountId(), checkToken.getCompanyId());

                                if (isPermitted) {
//                                    response.object.setStatus(English.success);
//                                    response.object.setMessage(English.tokenValid);
                                    return new ResponseEntity(response.object, HttpStatus.CONTINUE);
                                } else {
                                    response.object.setMessage(English.userDoesntHaveAccess);
                                    return new ResponseEntity(response.object, HttpStatus.OK);
                                }
                            }
                        } else {
                            if (tokenExpired == null) {
                                response.object.setMessage(English.actionExpired);
                                return new ResponseEntity(response.object, HttpStatus.OK);
                            }

                            if (date.compareTo(tokenExpired) > 0) {
                                response.object.setMessage(English.actionExpired);
                                return new ResponseEntity(response.object, HttpStatus.OK);
                            } else {
                                Boolean isPermitted = isPermitted(request, menuId, action, companyId, "console");
                                serviceFunction.generateLastAction(checkToken.getAccountId(), checkToken.getCompanyId());

                                if (isPermitted) {
//                                    response.object.setStatus(English.success);
//                                    response.object.setMessage(English.actionValid);
                                    return new ResponseEntity(response.object, HttpStatus.CONTINUE);
                                } else {
                                    response.object.setMessage(English.userDoesntHaveAccess);
                                    return new ResponseEntity(response.object, HttpStatus.OK);
                                }
                            }
                        }
                    }

                } else {
                    response.object.setMessage(English.tokenInvalid);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }
            } else {
                response.object.setMessage(English.tokenNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }

        } else {
            response.object.setMessage(English.tokenNull);
            return new ResponseEntity(response.object, HttpStatus.OK);
        }
    }

}
