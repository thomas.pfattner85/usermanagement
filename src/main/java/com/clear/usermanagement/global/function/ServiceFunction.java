package com.clear.usermanagement.global.function;

import com.clear.usermanagement.constants.GlobalVariable;
import com.clear.usermanagement.entity.master.AccountEntity;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.clear.usermanagement.entity.master.TokenConfigEntity;
import com.clear.usermanagement.global.query.CreateQuery;
import com.clear.usermanagement.query.activity.ActivityQuery;
import com.clear.usermanagement.repository.master.AccountRepository;
import com.clear.usermanagement.repository.master.TokenConfigRepository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ServiceFunction {

    public final JdbcTemplate jdbcTemplate;
    public final ActivityQuery activityQuery;
    public final AccountRepository accountRepository;
    public final TokenConfigRepository tokenConfigRepository;

    @Autowired
    public ServiceFunction(
            JdbcTemplate jdbcTemplate,
            ActivityQuery activityQuery,
            AccountRepository accountRepository,
            TokenConfigRepository tokenConfigRepository
    ) {
        this.jdbcTemplate = jdbcTemplate;
        this.activityQuery = activityQuery;
        this.accountRepository = accountRepository;
        this.tokenConfigRepository = tokenConfigRepository;
    }

    public String generatedPassword(String password, String salt) throws NoSuchAlgorithmException {
        String passwordEncripted;

        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt.getBytes(StandardCharsets.UTF_8));
        byte[] bytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        passwordEncripted = sb.toString();

        return passwordEncripted;
    }

    public String generatedToken(String password, String salt, Integer companyId) throws NoSuchAlgorithmException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date();
        String today = dateFormat.format(date);

        String tokenEncripted;
        if (companyId == null) {
            salt = salt + today + "secretKey";
        } else {
            TokenConfigEntity tokenConfig = tokenConfigRepository.findByCompanyId(companyId);
            salt = salt + today + tokenConfig.getPrivateKey();
        }

        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt.getBytes(StandardCharsets.UTF_8));
        byte[] bytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        tokenEncripted = sb.toString();

        return tokenEncripted;
    }

    public String generateUUID() {
        return UUID.randomUUID().toString();
    }

    public Integer generateID(String tableName) {
        String idTable = "";

        switch (tableName) {
            case "ref_account": idTable = "account_id"; break;
            case "ref_account_details": idTable = "account_details_id"; break;
            case "ref_activity_config": idTable = "activity_config_id"; break;
            case "ref_allowed_ip": idTable = "allowed_ip_id"; break;
            case "ref_allowed_ip_config": idTable = "allowed_ip_id_config_id"; break;
            case "ref_button": idTable = "button_id"; break;
            case "ref_company": idTable = "company_id"; break;
            case "ref_department": idTable = "department_id"; break;
            case "ref_email_config": idTable = "email_config_id"; break;
            case "ref_jobtitle": idTable = "jobtitle_id"; break;
            case "ref_menu": idTable = "menu_id"; break;
            case "ref_package": idTable = "package_id"; break;
            case "ref_privileges": idTable = "privileges_id"; break;
            case "ref_product": idTable = "product_id"; break;
            case "ref_role": idTable = "role_id"; break;
            case "ref_setting_config": idTable = "setting_config_id"; break;
            case "ref_token_config": idTable = "token_config_id"; break;
            case "dat_activity": idTable = "activity_id"; break;
            case "dat_menu_button": idTable = "menu_button_id"; break;
            case "dat_menu_company": idTable = "menu_company_id"; break;
            case "dat_menu_package": idTable = "menu_package_id"; break;
            case "dat_menu_role": idTable = "menu_role_id"; break;
            case "dat_privileges_menu_button": idTable = "privileges_menu_button_id"; break;
            case "dat_role_privileges": idTable = "role_privileges_id"; break;
            case "dat_user_fcm": idTable = "user_fcm_id"; break;
            case "dat_user_role": idTable = "user_role_id"; break;
        }

        try {
            String query = "SELECT " + idTable + " FROM " + tableName + " ORDER BY " + idTable + " DESC LIMIT 1";
            Integer allData = jdbcTemplate.queryForObject(query,
                    new Object[]{},
                    Integer.class);

            return (allData + 1);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 1;
    }

    public void generateActivity(Integer activityId, Integer accountId, String activity, String activityType, Integer moduleId, String modules, String url) {
        try {
            Connection conn = DriverManager.getConnection(GlobalVariable.URL_DATABASE);
            conn.setAutoCommit(false);

            try {
                String uuid = generateUUID();
                activityQuery.createActivity(conn, activityId, accountId, activity, activityType, moduleId, modules, url, uuid);
                conn.commit();

            } catch (SQLException sqlexcep) {
                sqlexcep.printStackTrace();
                conn.rollback();
            } finally {
                conn.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void generateLastAction(Integer accountId, Integer companyId) {
        if (companyId != null) {
            TokenConfigEntity tokenConfig = this.tokenConfigRepository.findByCompanyId(companyId);
            if (tokenConfig != null) {
                if (!"token".equals(tokenConfig.getExpiredFrom())) {
                    AccountEntity account = accountRepository.findByUserActivatedByAccountId(accountId, companyId);
                    if (account != null) {
                        Calendar cal = Calendar.getInstance();
                        if (null != tokenConfig.getTypeExpired()) {
                            switch (tokenConfig.getTypeExpired()) {
                                case "year":
                                    cal.add(Calendar.YEAR, tokenConfig.getExpired());
                                    break;
                                case "month":
                                    cal.add(Calendar.MONTH, tokenConfig.getExpired());
                                    break;
                                case "day":
                                    cal.add(Calendar.DATE, tokenConfig.getExpired());
                                    break;
                                case "hour":
                                    cal.add(Calendar.HOUR, tokenConfig.getExpired());
                                    break;
                                case "minute":
                                    cal.add(Calendar.MINUTE, tokenConfig.getExpired());
                                    break;
                                default:
                                    break;
                            }
                        }

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        String lastAction = formatter.format(cal.getTime());

                        account.setLastAction(lastAction);
                        accountRepository.save(account);
                    }
                }
            }
        }
    }

}
