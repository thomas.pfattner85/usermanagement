package com.clear.usermanagement.global.function;

import com.clear.usermanagement.entity.master.AccountEntity;
import com.clear.usermanagement.entity.master.ApiManagementEntity;
import com.clear.usermanagement.entity.master.MenuEntity;
import com.clear.usermanagement.entity.master.RoleEntity;
import com.clear.usermanagement.repository.master.AccountRepository;
import com.clear.usermanagement.repository.master.ApiManagementRepository;
import com.clear.usermanagement.repository.master.MenuRepository;
import com.clear.usermanagement.repository.master.RoleRepository;
import com.clear.usermanagement.repository.transaction.MenuButtonRepository;
import com.clear.usermanagement.repository.transaction.PrivilegesMenuButtonRepository;
import com.clear.usermanagement.repository.transaction.RolePrivilegesRepository;
import com.clear.usermanagement.repository.transaction.UserRoleRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class GetFunction {

    public final ServiceFunction serviceFunction;
    public final AccountRepository accountRepository;
    public final RoleRepository roleRepository;
    public final UserRoleRepository userRoleRepository;
    public final RolePrivilegesRepository rolePrivilegesRepository;
    public final PrivilegesMenuButtonRepository privilegesMenuButtonRepository;
    public final MenuButtonRepository menuButtonRepository;
    public final MenuRepository menuRepository;
    public final ApiManagementRepository apiManagementRepository;

    @Autowired
    public GetFunction(
            ServiceFunction serviceFunction,
            AccountRepository accountRepository,
            RoleRepository roleRepository,
            UserRoleRepository userRoleRepository,
            RolePrivilegesRepository rolePrivilegesRepository,
            PrivilegesMenuButtonRepository privilegesMenuButtonRepository,
            MenuButtonRepository menuButtonRepository,
            MenuRepository menuRepository,
            ApiManagementRepository apiManagementRepository
    ) {
        this.serviceFunction = serviceFunction;
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.userRoleRepository = userRoleRepository;
        this.rolePrivilegesRepository = rolePrivilegesRepository;
        this.privilegesMenuButtonRepository = privilegesMenuButtonRepository;
        this.menuButtonRepository = menuButtonRepository;
        this.menuRepository = menuRepository;
        this.apiManagementRepository = apiManagementRepository;
    }

    public RoleEntity getRoleSuperadmin() {
        RoleEntity roleSuperadmin = null;

        try {
            roleSuperadmin = roleRepository.findRoleSuperadmin();
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return roleSuperadmin;
    }

    public List<Integer> getRoleIdByAccountId(Integer accountId) {
        return userRoleRepository.findRoleIdByAccountId(accountId);
    }

    public AccountEntity getAccountSuperadmin(String username, String email) {
        AccountEntity accountSuperadmin = null;

        try {
            accountSuperadmin = accountRepository.findByUsernameAdmin(username) == null
                    ? accountRepository.findByUsernameAdmin(email)
                    : accountRepository.findByUsernameAdmin(username);

        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return accountSuperadmin;
    }

    public AccountEntity getAccountByAccountId(Integer accountId, Integer companyId) {
        AccountEntity account = null;

        try {
            account = accountRepository.findByUserActivatedByAccountId(accountId, companyId);

        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return account;
    }

    public AccountEntity getTokenAccount(String typeToken, String token) {
        AccountEntity account = null;

        try {
            account = "console".equals(typeToken)
                    ? accountRepository.findByToken(token)
                    : accountRepository.findByTokenMobile(token);

        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return account;
    }
    
    public AccountEntity getDataToken(HttpServletRequest request) {
        String token = request.getHeader("token");
        AccountEntity account = accountRepository.dataToken(token);
        
        return account;
    }

    public ApiManagementEntity getApiManagementUrl(String url) {
        ApiManagementEntity apiManagement = apiManagementRepository.findByUrl(url);

        if (apiManagement == null) {
            return null;
        }

        return apiManagement;
    }

    public Integer getMenuId(String menuClass) {
        MenuEntity menu = menuRepository.findByClassId(menuClass);

        if (menu == null) {
            return null;
        }

        return menu.getMenuId();
    }

}
