package com.clear.usermanagement.global.query;

import com.clear.usermanagement.entity.master.AccountEntity;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class GetQuery {

    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public GetQuery(
            JdbcTemplate jdbcTemplate
    ) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public AccountEntity getAccountSuperadminByUsernameOrEmail(String username, String email) {
        AccountEntity accountSuperadmin = null;

        try {
            String query = "SELECT username FROM ref_account WHERE (username = ? OR email = ?) AND banned = 0 AND activated = 1 AND status = 1";
            accountSuperadmin = jdbcTemplate.queryForObject(query,
                    new Object[]{
                        username,
                        email
                    },
                    new BeanPropertyRowMapper<>(AccountEntity.class));

        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return accountSuperadmin;
    }

    public List<String> getPrivilegesMenuButtonByRoleId(List<Integer> roleId) {
        List<String> privilegesMenuButton = new ArrayList<>();

        try {
            String query = "SELECT button_class_id FROM dat_menu_button "
                    + "WHERE menu_button_id in "
                    + "(SELECT menu_button_id FROM dat_privileges_menu_button "
                    + "WHERE privileges_id in "
                    + "(SELECT privileges_id FROM dat_role_privileges "
                    + "WHERE role_id in ? "
                    + "AND status = 1) "
                    + "AND status = 1) "
                    + "AND status = 1";
            privilegesMenuButton = jdbcTemplate.queryForList(query,
                    new Object[]{
                        roleId
                    },
                    String.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return privilegesMenuButton;
    }

}
