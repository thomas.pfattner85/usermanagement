package com.clear.usermanagement.global.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.stereotype.Service;

@Service
public class UpdateQuery {
    
    public void updateAccountActivated(Connection conn, Integer accountId) throws SQLException {
        String query = "UPDATE ref_account SET activated = 1 where account_id = ?";

        PreparedStatement updateData = conn.prepareStatement(query);

        updateData.setInt(1, accountId);
        updateData.executeUpdate();
    }

}
