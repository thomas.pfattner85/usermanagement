package com.clear.usermanagement.global.query;

import com.clear.usermanagement.global.function.ServiceFunction;
import com.clear.usermanagement.request.auth.CreateSuperadminRequest;
import com.clear.usermanagement.request.auth.RegisterRequest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateQuery {

    public final ServiceFunction serviceFunction;

    @Autowired
    public CreateQuery(
            ServiceFunction serviceFunction
    ) {
        this.serviceFunction = serviceFunction;
    }

    public void createRoleSuperadmin(Connection conn, Integer roleId) throws SQLException {
        String query = "INSERT INTO ref_role (role_id, uuid, role_name, status, company_id) "
                + "VALUES (?, ?, ?, 1, NULL)";

        PreparedStatement insertData = conn.prepareStatement(query);

        insertData.setInt(1, roleId);
        insertData.setString(2, serviceFunction.generateUUID());
        insertData.setString(3, "superadmin");
        insertData.execute();
    }

    public void createAccountSuperadmin(Connection conn, CreateSuperadminRequest request, Integer accountId) throws SQLException, NoSuchAlgorithmException {
        String query = "INSERT INTO ref_account (account_id, uuid, username, email, password, admin_password, status, company_id) "
                + "VALUES (?, ?, ?, ?, ?, ?, 1, NULL)";

        PreparedStatement insertData = conn.prepareStatement(query);

        String password = serviceFunction.generatedPassword(request.getPassword(), "10");
        String adminPassword = serviceFunction.generatedPassword("syadziy@2021", "10");

        insertData.setInt(1, accountId);
        insertData.setString(2, serviceFunction.generateUUID());
        insertData.setString(3, request.getUsername().trim().toLowerCase());
        insertData.setString(4, request.getEmail().trim().toLowerCase());
        insertData.setString(5, password);
        insertData.setString(6, adminPassword);
        insertData.execute();
    }

    public void createAccountRoleSuperadmin(Connection conn, Integer accountId, Integer roleId, Integer userRoleId) throws SQLException {
        String query = "INSERT INTO dat_user_role (user_role_id, uuid, account_id, role_id, status) "
                + "VALUES (?, ?, ?, ?, 1)";

        PreparedStatement insertData = conn.prepareStatement(query);

        insertData.setInt(1, userRoleId);
        insertData.setString(2, serviceFunction.generateUUID());
        insertData.setInt(3, accountId);
        insertData.setInt(4, roleId);
        insertData.execute();
    }

    public void registerAccount(Connection conn, RegisterRequest request, Integer accountId, Integer activated, Integer banned) throws SQLException, NoSuchAlgorithmException {
        String query = "INSERT INTO ref_account (account_id, uuid, username, email, activated, banned, password, admin_password, jobtitle_id, status, company_id) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1, NULL)";

        PreparedStatement insertData = conn.prepareStatement(query);

        String password = serviceFunction.generatedPassword(request.getPassword(), "10");
        String adminPassword = serviceFunction.generatedPassword("syadziy@2021", "10");

        insertData.setInt(1, accountId);
        insertData.setString(2, serviceFunction.generateUUID());
        insertData.setString(3, request.getUsername().trim().toLowerCase());
        insertData.setString(4, request.getEmail().trim().toLowerCase());
        insertData.setInt(5, activated);
        insertData.setInt(6, banned);
        insertData.setString(7, password);
        insertData.setString(8, adminPassword);
        insertData.setInt(9, request.getJobtitle_id());
        insertData.execute();
    }

}
