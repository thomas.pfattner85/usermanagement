package com.clear.usermanagement.constants;

public class GlobalVariable {

    public static String PRIVATE_KEY_STRING = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALPUeSASJHC"
            + "4WHpW14f87rd9yfWUBcy8m5aXeeR7ilksYXB/HsLMQi34nxzoZpvuVLXrRNTql3K2k6xHG3+R5H0NkTU8vNhysX4Kqn"
            + "ZTnAKWXOdDHxvtiT6exk7/7TKga6p4ZaXSYLMM6x95N4+fA5Ni4wNAvzK+Wi9tA7pfblOxAgMBAAECgYBjI4XLOnVAd"
            + "0C/4VRdMgHgwsYuTvaLkocsFEIsxjbVRnRJcqtgtP7fHSuK4kx/NnsY96UwxEvN//2VgnYUdDfEFBMsILXfowmfaNFQ"
            + "T8wrRb7/JnWTr8zGrOUTUEzL00WIHfqxBTshUI6oRnJFHKZKcv+QaBtxtHADIR7bQJ5f4QJBAOdEKwnizDWdsIKrAQe"
            + "A4fXRoJ+6nrUCRkbV5akaaZiw8HUDVkwvlDBe9DpdyxXyjqD69AnYUYfo4SuSP6Kssn0CQQDHEAgzi86Wi2AABYPxOT"
            + "9dXxHwoj7bwqB4nos5/6oCYJbCVUtmpWPgcdyPXd+U2MtTxNzd9yZBPjGboMBdCJhFAkBvn2dmjiEif8oCR6dnh72Ie"
            + "HWeouSXWVGyPutqg2UUXdKxp7qvaweLkUkwIrUzEq/k4zuGxPIVN0H3Vc+BJPnBAkEAo22BHjig0i35Eet1SJ0Ubab4"
            + "xpOdgbTHJGAds+83d54vB7sZHVuZpRI13ypVvhFC1TJhloYrTRMvjTKr23WL9QJBAI5womb+4cKInMFoojCH9mA5SgM"
            + "Jw7/eegJV+JO397ia6O+LBnqaC68dPXfX4y/QRTzM8ZkKUtaFVdw6pRYIxR8=";

    public static String PUBLIC_KEY_STRING = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCz1HkgEiRwuFh6VteH/O63"
            + "fcn1lAXMvJuWl3nke4pZLGFwfx7CzEIt+J8c6Gab7lS160TU6pdytpOsRxt/keR9DZE1PLzYcrF+Cqp2U5wCllznQx8"
            + "b7Yk+nsZO/+0yoGuqeGWl0mCzDOsfeTePnwOTYuMDQL8yvlovbQO6X25TsQIDAQAB";
    
    
    public static String URL_DATABASE = "jdbc:postgresql://localhost/usermanagement?user=admin&password=syadziy&ssl=false";
    
    public static String LOCAL_URL = "http://localhost:7000/api/";
    public static String DEV_URL = "http://localhost:7001/api/";
    public static String PROD_URL = "http://localhost:7002/api/";

    public static String IMAGE_PATH = "/home/syadziy/github/image/";
    public static String IMAGE_URL = "http://localhost:7000/image/";

}
