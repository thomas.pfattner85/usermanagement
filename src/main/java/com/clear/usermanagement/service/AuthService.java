package com.clear.usermanagement.service;

import com.clear.usermanagement.request.auth.ActivatedAccountRequest;
import com.clear.usermanagement.request.auth.LoginRequest;
import com.clear.usermanagement.request.auth.RegisterRequest;
import com.clear.usermanagement.response.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;

public interface AuthService {
    
    ResponseEntity login(Response response, LoginRequest request) throws JsonProcessingException;
    ResponseEntity register(Response response, RegisterRequest request) throws JsonProcessingException;
    ResponseEntity activatedAccount(Response response, ActivatedAccountRequest request) throws JsonProcessingException;
    
}
