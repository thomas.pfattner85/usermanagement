package com.clear.usermanagement.service.impl;

import com.clear.usermanagement.dao.AccountDao;
import com.clear.usermanagement.request.account.AccountRequest;
import com.clear.usermanagement.response.Response;
import com.clear.usermanagement.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
    
    private final AccountDao accountDao;

    @Autowired
    public AccountServiceImpl(
            AccountDao accountDao
    ) {
        this.accountDao = accountDao;
    }

    @Override
    public ResponseEntity getAllData(Response response, String url) {
        return accountDao.getAllData(response, url);
    }
    
    @Override
    public ResponseEntity getDetailData(Response response, String url, String uuid) {
        return accountDao.getDetailData(response, url, uuid);
    }
    
    @Override
    public ResponseEntity createData(Response response, AccountRequest request, String url) {
        return accountDao.createData(response, request, url);
    }
    
}
