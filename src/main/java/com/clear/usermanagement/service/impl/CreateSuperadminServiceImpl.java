package com.clear.usermanagement.service.impl;

import com.clear.usermanagement.request.auth.CreateSuperadminRequest;
import com.clear.usermanagement.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.clear.usermanagement.service.CreateSuperadminService;
import com.clear.usermanagement.dao.CreateSuperadminDao;

@Service
public class CreateSuperadminServiceImpl implements CreateSuperadminService {
    
    private final CreateSuperadminDao createSuperadminDao;

    @Autowired
    public CreateSuperadminServiceImpl(
            CreateSuperadminDao createSuperadminDao
    ) {
        this.createSuperadminDao = createSuperadminDao;
    }

    @Override
    public ResponseEntity createSuperadmin(Response response, CreateSuperadminRequest request) {
        return createSuperadminDao.createSuperadmin(response, request);
    }

}
