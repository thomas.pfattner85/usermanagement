package com.clear.usermanagement.service.impl;

import com.clear.usermanagement.dao.AuthDao;
import com.clear.usermanagement.request.auth.ActivatedAccountRequest;
import com.clear.usermanagement.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.clear.usermanagement.request.auth.LoginRequest;
import com.clear.usermanagement.request.auth.RegisterRequest;
import com.clear.usermanagement.service.AuthService;

@Service
public class AuthServiceImpl implements AuthService {
    
    private final AuthDao authDao;

    @Autowired
    public AuthServiceImpl(
            AuthDao authDao
    ) {
        this.authDao = authDao;
    }

    @Override
    public ResponseEntity login(Response response, LoginRequest request) {
        return authDao.login(response, request);
    }
    
    @Override
    public ResponseEntity register(Response response, RegisterRequest request) {
        return authDao.register(response, request);
    }
    
    @Override
    public ResponseEntity activatedAccount(Response response, ActivatedAccountRequest request) {
        return authDao.activatedAccount(response, request);
    }

}
