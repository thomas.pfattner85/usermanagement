package com.clear.usermanagement.dao;

import com.clear.usermanagement.request.auth.ActivatedAccountRequest;
import com.clear.usermanagement.request.auth.LoginRequest;
import com.clear.usermanagement.request.auth.RegisterRequest;
import com.clear.usermanagement.response.Response;
import org.springframework.http.ResponseEntity;

public interface AuthDao {
    
    ResponseEntity login(Response response, LoginRequest request);
    ResponseEntity register(Response response, RegisterRequest request);
    ResponseEntity activatedAccount(Response response, ActivatedAccountRequest request);
    
}
