package com.clear.usermanagement.dao.impl;

import com.clear.usermanagement.constants.GlobalVariable;
import com.clear.usermanagement.entity.master.RoleEntity;
import com.clear.usermanagement.request.auth.CreateSuperadminRequest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.stereotype.Repository;
import com.clear.usermanagement.dao.CreateSuperadminDao;
import com.clear.usermanagement.global.function.GetFunction;
import com.clear.usermanagement.global.function.ServiceFunction;
import com.clear.usermanagement.language.English;
import com.clear.usermanagement.global.query.CreateQuery;
import com.clear.usermanagement.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Repository
public class CreateSuperadminDaoImpl implements CreateSuperadminDao {

    public final ServiceFunction serviceFunction;
    public final GetFunction getFunction;
    public final CreateQuery createQuery;

    @Autowired
    public CreateSuperadminDaoImpl(
            ServiceFunction serviceFunction,
            GetFunction getFunction,
            CreateQuery createQuery
    ) {
        this.serviceFunction = serviceFunction;
        this.getFunction = getFunction;
        this.createQuery = createQuery;
    }

    @Override
    public ResponseEntity createSuperadmin(Response response, CreateSuperadminRequest request) {
        try {
            Connection conn = DriverManager.getConnection(GlobalVariable.URL_DATABASE);
            conn.setAutoCommit(false);

            try {
                Integer roleId = serviceFunction.generateID("ref_role");
                String username = request.getUsername().trim().toLowerCase();
                String email = request.getEmail().trim().toLowerCase();

                // Cek role superadmin
                if (getFunction.getRoleSuperadmin() == null) {
                    createQuery.createRoleSuperadmin(conn, roleId);
                    conn.commit();
                }

                // Cek akun superadmin
                if (getFunction.getAccountSuperadmin(username, email) != null) {
                    response.object.setMessage(English.userExist);
                    return new ResponseEntity(response.object, HttpStatus.OK);

                } else {
                    Integer accountId = serviceFunction.generateID("ref_account");
                    createQuery.createAccountSuperadmin(conn, request, accountId);
                    RoleEntity roleAdmin = getFunction.getRoleSuperadmin();

                    if (roleAdmin == null) {
                        response.object.setMessage(English.roleNotFound);
                        return new ResponseEntity(response.object, HttpStatus.OK);
                    } else {
                        Integer userRoleId = serviceFunction.generateID("dat_user_role");
                        createQuery.createAccountRoleSuperadmin(conn, accountId, roleAdmin == null ? roleId : roleAdmin.getRoleId(), userRoleId);

                        serviceFunction.generateActivity(
                                serviceFunction.generateID("dat_activity"),
                                accountId,
                                "Create superadmin",
                                "create",
                                null,
                                "auth",
                                "api/create_superadmin"
                        );

                        conn.commit();
                        response.object.setStatus(English.success);
                        response.object.setMessage(English.superadminSuccess);
                        return new ResponseEntity(response.object, HttpStatus.OK);
                    }
                }

            } catch (SQLException sqlexcep) {
                conn.rollback();
                sqlexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NoSuchAlgorithmException nosuchexcep) {
                conn.rollback();
                nosuchexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } finally {
                conn.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
