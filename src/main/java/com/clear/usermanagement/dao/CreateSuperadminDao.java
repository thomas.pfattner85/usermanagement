package com.clear.usermanagement.dao;

import com.clear.usermanagement.request.auth.CreateSuperadminRequest;
import com.clear.usermanagement.response.Response;
import org.springframework.http.ResponseEntity;

public interface CreateSuperadminDao {
    
    ResponseEntity createSuperadmin(Response response, CreateSuperadminRequest request);
    
}
