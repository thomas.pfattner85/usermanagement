package com.clear.usermanagement.request.auth;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CreateSuperadminRequest {
    
    @NotBlank
    private String username;
    
    @NotBlank
    private String email;
    
    @NotBlank
    private String password;
    
}
