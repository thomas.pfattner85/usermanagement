package com.clear.usermanagement.request.auth;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ActivatedAccountRequest {
    
    @NotNull
    private Integer account_id;
    
    @NotNull
    private Integer company_id;
    
}
