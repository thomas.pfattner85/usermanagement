package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.RoleEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<RoleEntity, Integer> {
    
    @Query(value = "select r from RoleEntity r where roleName = 'superadmin' and status = '1'")
    public RoleEntity findRoleSuperadmin();
    
}
