package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.CompanyEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends PagingAndSortingRepository<CompanyEntity, Integer> {
    
    @Query(value = "select c from CompanyEntity c where companyId = :companyId and status = '1'")
    public CompanyEntity findByCompanyId(@Param("companyId") Integer companyId);
    
}
