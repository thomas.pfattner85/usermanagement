package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.ApiManagementEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiManagementRepository extends PagingAndSortingRepository<ApiManagementEntity, Integer> {
    
    @Query(value = "select am from ApiManagementEntity am where url = :url")
    public ApiManagementEntity findByUrl(@Param("url") String url);
    
}
