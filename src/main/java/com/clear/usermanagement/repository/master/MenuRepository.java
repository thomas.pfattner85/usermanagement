package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.MenuEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends PagingAndSortingRepository<MenuEntity, Integer> {
    
    @Query(value = "select m from MenuEntity m where classId = :classId")
    MenuEntity findByClassId(@Param("classId") String classId);
    
}
