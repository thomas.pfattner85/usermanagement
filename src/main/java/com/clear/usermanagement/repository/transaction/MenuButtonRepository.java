package com.clear.usermanagement.repository.transaction;

import com.clear.usermanagement.entity.transaction.MenuButtonEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuButtonRepository extends PagingAndSortingRepository<MenuButtonEntity, Integer> {
    
}
