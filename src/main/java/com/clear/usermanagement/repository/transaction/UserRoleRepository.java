package com.clear.usermanagement.repository.transaction;

import com.clear.usermanagement.entity.transaction.UserRoleEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends PagingAndSortingRepository<UserRoleEntity, Integer> {

    @Query(value = "select ur.roleId from UserRoleEntity ur where accountId = :accountId and status = '1'")
    public List<Integer> findRoleIdByAccountId(@Param("accountId") Integer accountId);

}
