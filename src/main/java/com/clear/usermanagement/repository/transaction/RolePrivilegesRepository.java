package com.clear.usermanagement.repository.transaction;

import com.clear.usermanagement.entity.transaction.RolePrivilegesEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePrivilegesRepository extends PagingAndSortingRepository<RolePrivilegesEntity, Integer> {
    
}
