package com.clear.usermanagement.repository.transaction;

import com.clear.usermanagement.entity.transaction.PrivilegesMenuButtonEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrivilegesMenuButtonRepository extends PagingAndSortingRepository<PrivilegesMenuButtonEntity, Integer> {
    
}
