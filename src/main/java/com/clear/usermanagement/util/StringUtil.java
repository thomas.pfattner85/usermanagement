package com.clear.usermanagement.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

public class StringUtil {

    private static final String PATTERN_DATE = "dd-MMM-yyy HH:mm:ss";

    public static boolean hasValue(Object o) {
        if (o == null) {
            return false;
        } else if (o.toString().trim().equals("")) {
            return false;
        } else if (o.toString().isEmpty()) {
            return false;
        }

        return true;
    }

    public static String concat(String s1, String s2) {
        return s1.concat(s2);
    }

    public static String concatDate(String s1, Date d1) {
        return concatDate(s1, d1, PATTERN_DATE);
    }

    public static String concatDate(String s1, Date d1, String datePattern) {
        return s1.concat(DateUtil.dateToString(d1, datePattern));
    }

    public String defaultString(String a) {
        return a.trim().toLowerCase();
    }

    public static double toDouble(String s) {
        return Double.parseDouble(s.trim());
    }

    public static int toInteger(String s) {
        return Integer.parseInt(s.trim());
    }

    public static long toLong(String s) {
        return Long.parseLong(s.trim());
    }

    public static String wrappedByWildCard(String s) {
        String wildCard = "%";
        if (StringUtil.hasValue(s)) {
            return wildCard + s + wildCard;
        }

        return wildCard;
    }

    public static final int LEFT = 0;
    public static final int RIGHT = 1;

    public static String formatNumber(String num) {
        return formatNumber(num, 8, 0);
    }

    public static String formatNumber(String num, int precision) {
        return formatNumber(num, precision, precision);
    }

    public static String formatNumber(String num, int precision, int minPrecision) {
        return formatNumber(num, precision, minPrecision, true);
    }

    public static String formatNumber(String num, int precision, int minPrecision, boolean useGrouping) {
        try {
            DecimalFormat nf = new DecimalFormat();

            nf.setMinimumFractionDigits(minPrecision);
            nf.setMaximumFractionDigits(precision);

            nf.setGroupingUsed(useGrouping);
            double dd = Double.parseDouble(num);

            if (dd < 0) {
                dd = dd * (-1); // make positive number
                num = "(" + nf.format(dd) + ")";
            } else {
                num = nf.format(dd);
            }

        } catch (NumberFormatException e) {
            return null;
        }

        return num;
    }

    public static String removeZeros(String value) {
        if (StringUtil.hasValue(value)) {
            try {
                long tmp = Long.parseLong(value.trim());
                return "" + tmp;
            } catch (NumberFormatException e) {
                return value;
            }
        }

        return "";
    }

    public static String left(String str, int len) {
        if (str == null) {
            return "";
        }
        if (str.length() > len) {
            return str.substring(0, len);
        }

        return str;
    }

    public static String right(String str, int len) {
        if (str == null) {
            return "";
        }
        if (str.length() > len) {
            return str.substring(str.length() - len, str.length());
        }

        return str;
    }

    public static String reFormat(String number) {
        String answer = null;
        try {
            java.text.DecimalFormat a = new java.text.DecimalFormat();
            answer = (new java.math.BigDecimal(a.parse(number).toString())).toString();
        } catch (ParseException e) {
            e.getMessage();
        }

        return answer;
    }

    public static String escapeQuote4SQL(String string) {
        if (string != null) {
            return StringUtil.replace(string, "'", "''");
        }

        return string;
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public static Vector<String> tokenized(String string2bTokenized, String separator) {
        StringTokenizer tokens = new StringTokenizer(string2bTokenized, separator);
        Vector<String> vectorReturned = new Vector();
        int count = tokens.countTokens();
        for (int i = 0; i < count; i++) {
            String temp = tokens.nextToken().trim();
            vectorReturned.addElement(temp);
        }

        return vectorReturned;
    }

    public static String nullStr(String str) {
        return nullStr(str, "");
    }

    public static String nullString(String str) {
        return nullStr(str, "");
    }

    public static String nullStr2(String str, String defaultString) {
        return replace(nullStr(str, defaultString), "'", "''");
    }

    public static String nullStr2(String str) {
        return replace(nullStr(str, ""), "'", "''");
    }

    public static String nullStr(String parameter, String defaultString) {
        return (parameter == null || parameter.equalsIgnoreCase("") || parameter.equalsIgnoreCase("null")) ? defaultString : parameter;
    }

    public static String nullStrDash(String str) {
        return nullStr(str, "--");
    }

    public static String nullObj(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    public static String nullFloat(String str) {
        return (!StringUtil.hasValue(str)) ? "0" : str;
    }

    public static String nullDate(Date date) {
        if (date == null) {
            return "--";
        }

        return date.toString();
    }

    public static boolean isNullStr(String str) {
        return !StringUtil.hasValue(str);
    }

    public static String pad(String str, int len, char c, int direction) {
        StringBuilder sb = new StringBuilder(str);
        if (direction == StringUtil.LEFT) {
            sb.reverse();
            for (int i = str.length(); i < len; i++) {
                sb.append(c);
            }
            sb.reverse();
        } else {
            for (int i = str.length(); i < len; i++) {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    public static String padZeros(String str, int len) {
        return (str.length() > len) ? str.substring(0, len) : pad(str, len, '0', StringUtil.LEFT);
    }

    public static String padZerosBack(String str, int len) {
        return (str.length() > len) ? str.substring(0, len) : pad(str, len, '0', StringUtil.RIGHT);
    }

    public static String padSpaceBack(String str, int len) {
        str = StringUtil.nullStr(str);
        return (str.length() > len) ? str.substring(0, len) : pad(str, len, ' ', StringUtil.RIGHT);
    }

    public static String padSpace(String str, int len) {
        str = StringUtil.nullStr(str);
        return (str.length() > len) ? str.substring(0, len) : pad(str, len, ' ', StringUtil.LEFT);
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static boolean isNumeric(String str) {
        try {
            new Double(nullFloat(str.trim()));
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    public static String replace2(String source, String from, String to) {
        StringBuffer result = new StringBuffer();
        int i = source.indexOf(from, 0);
        if (i >= 0) {
            result.append(source.substring(0, i));
            result.append(to);
            i += from.length();
        } else {
            i = 0;
        }
        result.append(source.substring(i));

        return new String(result);
    }

    public static String replace(String str, String pattern, String replace) {
        int s = 0;
        int e = 0;
        StringBuilder result = new StringBuilder();

        try {
            while ((e = str.indexOf(pattern, s)) >= 0) {
                result.append(str.substring(s, e));
                result.append(replace);
                s = e + pattern.length();
            }
            result.append(str.substring(s));
        } catch (Exception ee) {
            return null;
        }

        return result.toString();
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public static Vector<Object> array2Vector(Object[] array) {
        return new Vector(Arrays.asList(array));
    }

    public static String extractFilename(String file) {
        if (file.indexOf("/") == file.indexOf("\\")) {
            return file.replace(' ', '_');
        }
        int lastIndex = (file.lastIndexOf("\\") > file.lastIndexOf("/")) ? file.lastIndexOf("\\") : file.lastIndexOf("/");
        return file.substring(lastIndex + 1).replace(' ', '_');
    }

    public static String getClassName(Object o) {
        String classname = o.getClass().toString();
        return classname.substring(classname.lastIndexOf(".") + 1);
    }

    public static String replaceWildcard(String criteria) {
        if (isNullStr(criteria)) {
            return null;
        }
        if (criteria.contains("*")) {
            criteria = criteria.replace('*', '%');
        } else {
            // criteria = ("%") + criteria + ("%");
            criteria = criteria + ("%");
        }
        return criteria;
    }

    public static final String lineWrap(String str, int borderLine) {
        char NEW_LINE = '\n';
        char SPACE = ' ';
        int strLength = str.length();

        if (strLength > borderLine) {
            String sub = str.substring(0, borderLine);
            String sub2 = str.substring(borderLine);

            int spaceIdx = str.lastIndexOf(SPACE);

            // get the index of righmost occurence of space char before
            // borderline
            int spaceIdxA = sub.lastIndexOf(SPACE);

            // get the index of leftmost occurrence of space char after
            // borderline
            int spaceIdxB = sub2.indexOf(SPACE);

            int diff = (borderLine - spaceIdxA) - 1;

            // space found not at beginning or not exist
            if (spaceIdx > 0) {
                // space found before borderline
                if ((spaceIdxA >= 0) & (spaceIdxB == -1)) {
                    return rTrim(str.substring(0, spaceIdxA))
                            + NEW_LINE
                            + lineWrap(str.substring(spaceIdxA).trim(), borderLine);
                } // space found after borderline
                else if ((spaceIdxB >= 0) & (spaceIdxA == -1)) {
                    return rTrim(str.substring(0, (borderLine + spaceIdxB)))
                            + NEW_LINE
                            + lineWrap(str.substring((borderLine + spaceIdxB)).trim(), borderLine);
                } // space found before & after borderline
                else if ((spaceIdxA >= 0) && (spaceIdxB >= 0)) {
                    if (diff <= spaceIdxB) {
                        return rTrim(str.substring(0, spaceIdxA))
                                + NEW_LINE
                                + lineWrap(str.substring(spaceIdxA).trim(), borderLine);
                    } else if (diff > spaceIdxB) {
                        return rTrim(str.substring(0, (borderLine + spaceIdxB)))
                                + NEW_LINE
                                + lineWrap(str.substring(
                                        (borderLine + spaceIdxB)).trim(), borderLine);
                    }
                }
            }
            // no space character found or at the beginning or found but not in the conditional
            return rTrim(str.substring(0, borderLine - 3))
                    + NEW_LINE
                    + lineWrap(str.substring((borderLine - 3)).trim(), borderLine);
        }

        return str;
    }

    public static String rTrim(String string) {
        String WHITESPACE_CHARS = " \t\n\f\r";
        if (string.length() == 0) {
            return string;
        }
        StringBuffer sb = new StringBuffer(string).reverse();
        while (WHITESPACE_CHARS.indexOf(sb.charAt(0)) >= 0) {
            sb.deleteCharAt(0);
        }

        return sb.reverse().toString();
    }

    public static String[] subString(String string, int numOfChar, int numOfString) {
        String[] stringArr = new String[numOfString];
        int length = string.length();
        int lengthRemain = 0;
        int beginIndex = 0;
        int endIndex = numOfChar - 1;
        int i = 0;
        boolean stop = false;

        while ((i < numOfString) && (!stop)) {
            if (length > numOfChar) {
                stringArr[i] = string.substring(beginIndex, endIndex);
                beginIndex = endIndex + 1;
                lengthRemain = length - ((i + 1) * numOfChar);
                if (lengthRemain > numOfChar) {
                    endIndex = endIndex + numOfChar;
                } else {
                    endIndex = endIndex + lengthRemain;
                }
                i++;
            } else {
                stringArr[i] = string;
                stop = true;
            }
        }

        return stringArr;
    }

    public static <T> T nvl(T arg0, T arg1) {
        return (arg0 == null) ? arg1 : arg0;
    }

    public static String toPercent(Double val) {
        if (Double.isNaN(val) || Double.isInfinite(val)) {
            return "0.0%";
        }
        return String.valueOf(val * 100) + "%";
    }

}
